#!/bin/bash
num=0
str="=>"
max=100


while [ "$num" -le "$max" ]
do
	let index=num%4
	printf "[%-52s %-2d%% %c]\r" "${str}" "${num}"
	let num++
	sleep 0.1
	if [[ "$num%2" -eq 0 ]];
	then
	str='='${str}
	fi
done
echo
