#!/bin/bash
t=`java -version`
if [[ -z $t ]];
then 
	mkdir -p /usr/local/src/jdk
	wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz
	tar -zxvf /usr/local/src/jdk/jdk-8u131-linux-x64.tar.gz -C/usr/local/src/jdk
	echo "export JAVA_HOME=/usr/local/src/jdk/jdk1.8.0_131" >>/etc/profile
	echo "export PATH=$PATH:$JAVAHOME/bin" >>etc/profile
	source /etc/profile
fi
